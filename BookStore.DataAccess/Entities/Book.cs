﻿using BookStore.DataAccess.Entities.Base;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookStore.DataAccess.Entities
{
    public class Book : BaseEntity
    {
        [Required]
        [MinLength(3)]
        [Column(TypeName = "nvarchar(30)")]
        public string Name { get; set; }

        [Required]
        public decimal Price { get; set; }

        [Required]
        public int Published { get; set; }

        public Guid CreatorId { get; set; }
        [ForeignKey("CreatorId")]
        public Creator Creator { get; set; }

        public List<Order> Orders { get; set; }

        public Book()
        {
            Orders = new List<Order>();
        }
    }
}
