﻿using BookStore.DataAccess.Entities.Base;
using BookStore.DataAccess.Entities.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BookStore.DataAccess.Entities
{
    public class Seller : BaseEntity
    {
        [Required]
        [MinLength(3)]
        [Column(TypeName = "nvarchar(30)")]
        public string Surname { get; set; }

        [Required]
        [MinLength(3)]
        [Column(TypeName = "nvarchar(30)")]
        public string FirstName { get; set; }

        [MinLength(3)]
        [Column(TypeName = "nvarchar(30)")]
        public string SecondName { get; set; }

        public Gender Gender { get; set; }

        public List<Order> Orders { get; set; }

        public Seller()
        {
            Orders = new List<Order>();
        }
    }
}
