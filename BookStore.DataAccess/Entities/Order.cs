﻿using BookStore.DataAccess.Entities.Base;
using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace BookStore.DataAccess.Entities
{
    public class Order : BaseEntity
    {
        public Guid CustomerId { get; set; }
        [ForeignKey("CustomerId")]
        public Customer Customer { get; set; }

        public Guid BookId { get; set; }
        [ForeignKey("BookId")]
        public Book Book { get; set; }

        public Guid SellerId { get; set; }
        [ForeignKey("SellerId")]
        public Seller Seller  { get; set; }
    }
}
