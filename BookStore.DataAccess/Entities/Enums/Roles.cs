﻿

namespace BookStore.Core.Security
{
    public enum Roles
    {
        None = 0,
        Administrator = 1,
        Client = 2
    }
}
