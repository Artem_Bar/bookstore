﻿
using BookStore.Core.Security;
using Microsoft.AspNetCore.Identity;

namespace BookStore.DataAccess.Entities.Identity
{
    public class AppUser : IdentityUser
    {
        public string Surname { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        //public Roles Role { get; set; }
    }
}
