﻿using System;
using System.ComponentModel.DataAnnotations;

namespace BookStore.DataAccess.Entities.Base
{
    public class BaseEntity
    {
        [Key]
        public Guid Id { get; set; }

        public DateTime CreationDate { get; set; }

        public BaseEntity()
        {
            Id = new Guid();

            CreationDate = DateTime.UtcNow;
        }
    }
}
