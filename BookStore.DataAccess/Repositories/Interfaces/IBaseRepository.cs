﻿using BookStore.DataAccess.Entities.Base;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BookStore.DataAccess.Repositories.Interfaces
{
    interface IBaseRepository<T> where T : BaseEntity
    {
        Task<IEnumerable<T>> GetAll();
        Task<T> FindById(Guid id);
        Task Insert(T entity);
        Task Delete(Guid id);
        Task Update(T entity);
    }
}
