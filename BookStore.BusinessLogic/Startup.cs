﻿using BookStore.BusinessLogic.Services;
using BookStore.BusinessLogic.Services.Interfaces;
using BookStore.DataAccess;
using BookStore.DataAccess.Entities.Identity;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace BookStore.BusinessLogic
{
    public class Startup
    {
        public static void Configure(IServiceCollection services, string connectionString)
        {
            ConfigureDbContext(services, connectionString);
            ConfigureIdentity(services);
            RegisterServicesDependencies(services, connectionString);
        }
        public static void ConfigureDbContext(IServiceCollection services, string connectionString)
        {
            services.AddDbContext<AppDbContext>(options =>
                options.UseSqlServer(connectionString, b => b.MigrationsAssembly("BookStore.DataAccess")));
        }

        public static void ConfigureIdentity(IServiceCollection services)
        {
            services.AddIdentity<AppUser, IdentityRole>().AddEntityFrameworkStores<AppDbContext>().AddDefaultTokenProviders();
        }

        public static void RegisterServicesDependencies(IServiceCollection services, string connectionString)
        {
            services.AddScoped<IDbConnection, SqlConnection>(c => new SqlConnection(connectionString));
            services.AddScoped<IAccountService, AccountService>();
        }

    }
}
