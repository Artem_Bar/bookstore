﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace BookStore.Api.Controllers
{
    [Route("[controller]/[action]")]
    public class TestController : Controller
    {
        [HttpGet]
        public async Task<IActionResult> TestCall(string str)
        {
            string responseMessage = "access";
            if (string.IsNullOrEmpty(str))
            {
                responseMessage = "no access";
                return Ok(responseMessage);
            }
            return Ok(new { responseMessage });
        }
    }
}
